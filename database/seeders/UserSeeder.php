<?php

namespace Database\Seeders;

use App\Models\Currency;
use App\Models\User;
use App\Models\Wallet;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $currencyIds = Currency::pluck('id');
        $faker = Factory::create();

        User::factory(5)->create()->each(function ($user) use ($currencyIds, $faker) {
            $currencyIds->each(function ($currencyId) use ($user, $faker) {
                Wallet::create([
                    'user_id' => $user->id,
                    'currency_id' => $currencyId,
                    'balance' => $faker->numberBetween(1000, 1000000),
                ]);
            });
        });
    }
}
