<?php

namespace App\Rules;

use App\Models\Request;
use App\Models\Wallet;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class BalanceEnough implements ValidationRule
{

    public function __construct(protected Wallet $wallet)
    {
    }

    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (isset($this->wallet) && $this->wallet->balance <= $value * Request::WITHDRAWAL_FEE) {
            $fail('You have not enough balance');
        }
    }
}
