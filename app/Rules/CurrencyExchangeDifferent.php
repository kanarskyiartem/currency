<?php

namespace App\Rules;

use App\Models\Wallet;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CurrencyExchangeDifferent implements ValidationRule
{
    public function __construct(protected Wallet|null $wallet)
    {
    }

    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (isset($this->wallet) && $this->wallet->currency_id == $value) {
            $fail('You can not exchange between same currency');
        }
    }
}
