<?php


namespace App\Actions;

use App\Events\ExchangeEvent;
use App\Models\Request as ModelRequest;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ApplyExchangeAction
{
    public function handle(ModelRequest $modelRequest)
    {
        $walletBuyer = Auth::user()->wallets()->where('currency_id', $modelRequest->buy_currency_id)->first();

        $this->validate($modelRequest, $walletBuyer);

        DB::beginTransaction();
        try {
            $this->walletBuyer($modelRequest);
            $this->walletSeller($modelRequest);
            $modelRequest->is_exchange = true;
            $modelRequest->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, 'Problem with transaction');
        }


    }

    private function validate(ModelRequest $modelRequest, Wallet|null $wallet)
    {
        if (!isset($wallet)) {
            abort(400, 'You have not wallet with this currency');
        }

        if ($modelRequest->wallet->user_id == Auth::id()) {
            abort(400, 'You can not select own request');
        }

        if ($modelRequest->amount_buy * ModelRequest::WITHDRAWAL_FEE >= $wallet->balance) {
            abort(400, 'You balance is not enough for this operation');
        }
    }

    private function walletBuyer(ModelRequest $modelRequest): void
    {
        $appendWallet = $this->findOrCreateWallet($modelRequest->buy_currency_id, Auth::id());
        $deductWallet = $this->findOrCreateWallet($modelRequest->wallet->currency_id, Auth::id());

        $feeAmount = $modelRequest->amount_buy * ModelRequest::WITHDRAWAL_FEE - $modelRequest->amount_buy;
        event(new ExchangeEvent($deductWallet->currency_id, $feeAmount));

        $deductWallet->balance = $deductWallet->balance - $modelRequest->amount_buy * ModelRequest::WITHDRAWAL_FEE;
        $appendWallet->balance = $appendWallet->balance + $modelRequest->amount_sell;
        $deductWallet->save();
        $appendWallet->save();

    }

    private function walletSeller(ModelRequest $modelRequest): void
    {
        $deductWallet = $modelRequest->wallet;
        $appendWallet = $this->findOrCreateWallet($modelRequest->buy_currency_id, $modelRequest->wallet->user_id);

        $feeAmount = $modelRequest->amount_sell * ModelRequest::WITHDRAWAL_FEE - $modelRequest->amount_sell;
        event(new ExchangeEvent($deductWallet->currency_id, $feeAmount));

        $deductWallet->balance = $deductWallet->balance - $modelRequest->amount_sell * ModelRequest::WITHDRAWAL_FEE;
        $appendWallet->balance = $appendWallet->balance + $modelRequest->amount_buy;
        $deductWallet->save();
        $appendWallet->save();
    }

    private function findOrCreateWallet(int $currencyId, int $userId): Builder|Model
    {
        return Wallet::query()->firstOrCreate(['currency_id' => $currencyId, 'user_id' => $userId],
            ['currency_id' => $currencyId, 'user_id' => $userId]);
    }
}
