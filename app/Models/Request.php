<?php

namespace App\Models;

use App\Models\Traits\Money;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Request extends Model
{
    use HasFactory, Money;

    protected $fillable = [
        'wallet_id',
        'buy_currency_id',
        'amount_sell',
        'amount_buy',
        'is_exchange'
    ];

    const WITHDRAWAL_FEE = 1.02;  //percent

    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    public function buyCurrency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'buy_currency_id', 'id');
    }

    public function amountSell(): Attribute
    {
        return $this->money();
    }

    public function amountBuy(): Attribute
    {
        return $this->money();
    }

    public function scopeNotOwnAndNotExchanged(Builder $query)
    {
        $query->select('requests.id', 'wallet_id', 'buy_currency_id', 'amount_buy',
            'amount_sell', 'is_exchange', 'requests.created_at')
            ->leftJoin('wallets', 'requests.wallet_id', 'wallets.id')
            ->where('wallets.user_id', '<>', Auth::id())
            ->where('is_exchange', 0);
    }
}
