<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use App\Models\Traits\Money;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class History extends Model
{
    use HasFactory, Money, Filterable;

    protected $fillable = ['amount', 'currency_id'];

    public function amount(): Attribute
    {
        return $this->money();
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }
}
