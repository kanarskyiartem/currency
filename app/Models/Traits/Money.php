<?php


namespace App\Models\Traits;


use Illuminate\Database\Eloquent\Casts\Attribute;

trait Money
{
    public function money(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value / 100,
            set: fn($value) => $value * 100
        );
    }
}
