<?php

namespace App\Listeners;

use App\Models\History;

class ExchangeHistory
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        History::create(['currency_id' => $event->currencyId, 'amount' => $event->amount]);
    }
}
