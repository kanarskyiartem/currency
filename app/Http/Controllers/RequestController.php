<?php

namespace App\Http\Controllers;

use App\Actions\ApplyExchangeAction;
use App\Http\Requests\StoreRequestRequest;
use App\Models\Request as ModelRequest;
use Illuminate\Http\JsonResponse;

class RequestController extends Controller
{
    public function index()
    {
        return ModelRequest::query()->notOwnAndNotExchanged()->get();
    }

    public function store(StoreRequestRequest $request): JsonResponse
    {
        ModelRequest::create($request->validated());

        return response()->json(['message' => 'Request was created']);
    }

    public function apply(ModelRequest $modelRequest, ApplyExchangeAction $action): JsonResponse
    {
        $modelRequest->load('wallet:id,user_id,currency_id,balance');
        $action->handle($modelRequest);

        return response()->json(['message' => 'Money was exchanged']);
    }
}
