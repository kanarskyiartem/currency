<?php

namespace App\Http\Controllers;

use App\Http\Filters\HistoryFilter;
use App\Http\Requests\HistoryRequest;
use App\Http\Resources\HistoryResource;
use App\Models\History;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class HistoryController extends Controller
{
    public function index(HistoryRequest $historyRequest): AnonymousResourceCollection
    {
        $data = $historyRequest->validated();
        $filter = app()->make(HistoryFilter::class, ['queryParams' => array_filter($data)]);
        $histories = History::filter($filter)->with('currency:id,name')->get();

        return HistoryResource::collection($histories);
    }
}
