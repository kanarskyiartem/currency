<?php

namespace App\Http\Requests;

use App\Models\Wallet;
use App\Rules\BalanceEnough;
use App\Rules\CheckOwnerOfWallet;
use App\Rules\CurrencyExchangeDifferent;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $wallet = Wallet::find(request()->get('wallet_id'));

        return [
            'wallet_id' => ['required', 'integer', 'exists:wallets,id', new CheckOwnerOfWallet()],
            'buy_currency_id' => ['required', 'integer', 'exists:currencies,id', new CurrencyExchangeDifferent($wallet)],
            'amount_sell' => ['required', 'numeric', 'min:1', new BalanceEnough($wallet)],
            'amount_buy' => 'required|numeric|min:1',
        ];
    }
}
