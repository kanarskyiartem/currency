<?php


namespace App\Http\Filters;


use Illuminate\Database\Eloquent\Builder;

class HistoryFilter extends AbstractFilter
{
    public const FROM = 'date_from';
    public const TO = 'date_to';

    protected function getCallbacks(): array
    {
        return [
            self::FROM => [$this, 'from'],
            self::TO => [$this, 'to'],
        ];
    }

    public function from(Builder $builder, $value)
    {
        $builder->whereDate('histories.created_at', '>=', $value);
    }

    public function to(Builder $builder, $value)
    {
        $builder->whereDate('histories.created_at', '<=', $value);
    }
}
